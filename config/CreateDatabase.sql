BEGIN;
CREATE DATABASE IF NOT EXISTS `spring_db_test` DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;

CREATE DATABASE IF NOT EXISTS `spring_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
USE `spring_db`;

DROP TABLE IF EXISTS `seat`;
DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `customer_number` int(11) NOT NULL,
  PRIMARY KEY (`p_key`),
  UNIQUE KEY `UNIQUE_CUSTOMER_NUMBER` (`customer_number`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

CREATE TABLE `seat` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `seat_number` int(11) NOT NULL,
  `customer_p_key` int(11) DEFAULT NULL,
  PRIMARY KEY (`p_key`),
  UNIQUE KEY `UNIQUE_SEAT_NUMBER` (`seat_number`),
  KEY `INDEX_SEAT` (`seat_number`),
  CONSTRAINT `seat_customer` FOREIGN KEY (`customer_p_key`) REFERENCES `customer` (`p_key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

DROP PROCEDURE IF EXISTS `init_spring`;

DELIMITER $$  
CREATE PROCEDURE init_spring()

BEGIN
	DECLARE seat_number INT DEFAULT 1 ;
	DECLARE customer_number INT DEFAULT 1 ;

	INIT_LOOP: LOOP         
		INSERT INTO `seat` (create_date, update_date, version, seat_number) VALUES(NOW(), NOW(), 0, seat_number);
		SET seat_number = seat_number + 1;
		IF seat_number = 100000 THEN
			LEAVE INIT_LOOP;
		END IF;
	END LOOP INIT_LOOP;

	INIT_LOOP: LOOP         
		INSERT INTO `customer` (create_date, update_date, version, customer_number) VALUES(NOW(), NOW(), 0, customer_number);
		SET customer_number = customer_number + 1;
		IF customer_number = 10 THEN
			LEAVE INIT_LOOP;
		END IF;
	END LOOP INIT_LOOP;

END $$
DELIMITER ;


CALL `init_spring`;
DROP PROCEDURE `init_spring`;
COMMIT;