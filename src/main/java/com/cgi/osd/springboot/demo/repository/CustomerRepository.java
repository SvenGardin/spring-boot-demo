package com.cgi.osd.springboot.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

	public boolean existsByCustomerNumber(int customerNumber);

	@Override
	public List<Customer> findAll();

	public Optional<Customer> findByCustomerNumber(int customerNumber);

}
