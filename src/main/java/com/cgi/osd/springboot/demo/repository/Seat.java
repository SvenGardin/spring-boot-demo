package com.cgi.osd.springboot.demo.repository;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 * The persistent class for the seat database table.
 *
 */
@Entity
public class Seat extends EntityBase implements Serializable, Comparable<Seat> {
	private static final long serialVersionUID = 1L;

	@Column(name = "seat_number")
	private int seatNumber;

	@ManyToOne
	private Customer customer;

	@Override
	public int compareTo(Seat otherSeat) {
		if (seatNumber < otherSeat.seatNumber) {
			return -1;
		}
		if (seatNumber > otherSeat.seatNumber) {
			return 1;
		}
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Seat other = (Seat) obj;
		return seatNumber == other.seatNumber;
	}

	public Customer getCustomer() {
		return customer;
	}

	public int getSeatNumber() {
		return seatNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + seatNumber;
		return result;
	}

	@Transient
	public boolean isBooked() {
		return customer != null;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void setSeatNumber(int seatNumber) {
		this.seatNumber = seatNumber;
	}

}