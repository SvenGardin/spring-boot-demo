package com.cgi.osd.springboot.demo.repository;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@MappedSuperclass
public class EntityBase {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "p_key")
	protected int pKey;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	protected Date createDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	protected Date updateDate;

	@Version
	private int version;


	public EntityBase() {
		super();
	}

	public Date getCreateDate() {
		return createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public int getVersion() {
		return version;
	}

	@PrePersist
	public void initCreateDate() {
		setCreateDate(new Date());
		initUpdateDate();
	}

	@PreUpdate
	public void initUpdateDate() {
		setUpdateDate(new Date());
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public void setVersion(int version) {
		this.version = version;
	}



}