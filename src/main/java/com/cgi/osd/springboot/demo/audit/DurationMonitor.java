/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cgi.osd.springboot.demo.audit;

import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

/**
 * This class is responsible for logging the execution time for methods annotated with @DurationLogger.
 *
 */
@Aspect
@Component
public class DurationMonitor {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Around("execution(@com.cgi.osd.springboot.demo.audit.DurationLogger * *(..)) && @annotation(durationLoggerAnnotation)")
	public Object logExecutionTime(ProceedingJoinPoint joinPoint, DurationLogger durationLoggerAnnotation)
			throws Throwable {
		final String name = createInvocationTraceName(joinPoint);
		final StopWatch stopWatch = new StopWatch(name);
		stopWatch.start(name);
		try {
			return joinPoint.proceed();
		} finally {
			stopWatch.stop();
			if (logger.isDebugEnabled()) {
				logger.debug(stopWatch.shortSummary());
			}
		}

	}

	private String createInvocationTraceName(ProceedingJoinPoint joinPoint) {
		final StringBuilder sb = new StringBuilder();
		final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		final Method method = signature.getMethod();
		final Class<?> clazz = method.getDeclaringClass();
		sb.append(clazz.getName());
		sb.append('.').append(method.getName());
		return sb.toString();
	}

}
