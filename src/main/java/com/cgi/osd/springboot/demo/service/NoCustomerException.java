package com.cgi.osd.springboot.demo.service;

public class NoCustomerException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final int customerNumber;

	public NoCustomerException(int customerNumber) {
		this.customerNumber = customerNumber;
	}

	public int getCustomerNumber() {
		return customerNumber;
	}

}
