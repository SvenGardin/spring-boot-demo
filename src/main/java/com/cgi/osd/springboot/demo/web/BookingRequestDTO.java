package com.cgi.osd.springboot.demo.web;

public class BookingRequestDTO {

	private int customerNumber;
	private int numberOfSeats;

	public BookingRequestDTO() {

	}

	public BookingRequestDTO(int customerNumber, int numberOfSeats) {
		this.customerNumber = customerNumber;
		this.numberOfSeats = numberOfSeats;
	}

	public int getCustomerNumber() {
		return customerNumber;
	}
	public int getNumberOfSeats() {
		return numberOfSeats;
	}
	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}
	public void setNumberOfSeats(int numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

}
