package com.cgi.osd.springboot.demo.service;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cgi.osd.springboot.demo.domain.BookingDO;
import com.cgi.osd.springboot.demo.domain.CustomerDO;

/**
 * This class defines all methods for booking operations.
 *
 */
public interface BookingService {

	public void createBookings(int customerNumber, int numberOfSeats);

	public CompletableFuture<Void> createBookingsAsync(int customerNumber, int numberOfSeats);

	public List<CustomerDO> getAllCustomers();

	public Collection<BookingDO> getBookings(int customerNumber, int maxLimit);

	public Page<BookingDO> getPagedBookings(int customerNumber, Pageable page);
}
