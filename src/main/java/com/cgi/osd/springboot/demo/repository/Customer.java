package com.cgi.osd.springboot.demo.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The persistent class for the customer database table.
 *
 */
@Entity
public class Customer extends EntityBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "customer_number")
	private int customerNumber;

	@OneToMany(mappedBy = "customer")
	private List<Seat> seats;

	public int getCustomerNumber() {
		return customerNumber;
	}

	public int getPKey() {
		return pKey;
	}

	public List<Seat> getSeats() {
		return seats;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}

	public void setSeats(List<Seat> seats) {
		this.seats = seats;
	}

}