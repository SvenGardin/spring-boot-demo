package com.cgi.osd.springboot.demo.repository;

import java.util.Optional;

import javax.persistence.LockModeType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface SeatRepository extends CrudRepository<Seat, Long> {

	@Query("SELECT s FROM Seat s WHERE s.customer.customerNumber = :customerNumber ORDER BY updateDate DESC")
	public Page<Seat> findByCustomer(@Param("customerNumber") int customerNumber, Pageable pageable);

	public Optional<Seat> findBySeatNumber(int seatNumber);

	@Query("SELECT s FROM Seat s WHERE s.customer IS NULL")
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	public Page<Seat> findFreeSeats(Pageable pageable);

	@Query("SELECT CASE WHEN COUNT(s) = 0 THEN true ELSE false END FROM Seat s WHERE s.seatNumber = :seatNumber AND s.customer IS NULL")
	public boolean isSeatFree(@Param("seatNumber") int seatNumber);

	long countByCustomerIsNull();
}
