package com.cgi.osd.springboot.demo.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cgi.osd.springboot.demo.audit.DurationLogger;
import com.cgi.osd.springboot.demo.domain.BookingDO;
import com.cgi.osd.springboot.demo.domain.CustomerDO;
import com.cgi.osd.springboot.demo.domain.RepositoryMapper;
import com.cgi.osd.springboot.demo.repository.Customer;
import com.cgi.osd.springboot.demo.repository.CustomerRepository;
import com.cgi.osd.springboot.demo.repository.Seat;
import com.cgi.osd.springboot.demo.repository.SeatRepository;

@Service
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
public class BookingServiceImpl implements BookingService {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private SeatRepository seatRepository;

	@Autowired
	private RepositoryMapper repositoryMapper;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE)
	@DurationLogger
	public void createBookings(int customerNumber, int numberOfSeats) {
		performCreateBookings(customerNumber, numberOfSeats);
	}

	@Override
	@Async("bookingSvc")
	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE)
	@DurationLogger
	public CompletableFuture<Void> createBookingsAsync(int customerNumber, int numberOfSeats) {
		performCreateBookings(customerNumber, numberOfSeats);
		return CompletableFuture.completedFuture(null);
	}

	@Override
	@DurationLogger
	public List<CustomerDO> getAllCustomers() {
		final List<Customer> customers = customerRepository.findAll();
		return repositoryMapper.customersToCustomerDOs(customers);
	}

	@Override
	@DurationLogger
	public Collection<BookingDO> getBookings(int customerNumber, int maxLimit) {
		if (!customerRepository.existsByCustomerNumber(customerNumber)) {
			throw new NoCustomerException(customerNumber);
		}
		final List<Seat> bookedSeats = seatRepository.findByCustomer(customerNumber, PageRequest.of(0, maxLimit))
				.getContent();
		return repositoryMapper.seatsToBookingDOs(bookedSeats);
	}

	@Override
	@DurationLogger
	public Page<BookingDO> getPagedBookings(int customerNumber, Pageable page) {
		if (!customerRepository.existsByCustomerNumber(customerNumber)) {
			throw new NoCustomerException(customerNumber);
		}
		final Page<Seat> bookedSeatsPage = seatRepository.findByCustomer(customerNumber, page);
		return repositoryMapper.seatsToPagedBookingDOs(bookedSeatsPage);
	}

	private void performCreateBookings(int customerNumber, int numberOfSeats) {
		final Optional<Customer> optionalCustomer = customerRepository.findByCustomerNumber(customerNumber);
		if (!optionalCustomer.isPresent()) {
			throw new NoCustomerException(customerNumber);
		}
		final Customer customer = optionalCustomer.get();

		final List<Seat> freeSeats = seatRepository.findFreeSeats(PageRequest.of(0, numberOfSeats)).getContent();
		if (freeSeats.size() < numberOfSeats) {
			final int actualNumberOfSeats = (int) seatRepository.countByCustomerIsNull();
			throw new NoFreeSeatException(numberOfSeats, actualNumberOfSeats);
		}

		for (final Seat seat : freeSeats) {
			seat.setCustomer(customer);
		}

	}
}
