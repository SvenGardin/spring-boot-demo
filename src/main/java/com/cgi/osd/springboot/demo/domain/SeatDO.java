package com.cgi.osd.springboot.demo.domain;

public class SeatDO {

	private int seatNumber;

	public int getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(int seatNumber) {
		this.seatNumber = seatNumber;
	}

}
