package com.cgi.osd.springboot.demo.domain;

public class CustomerDO {

	private int customerNumber;

	public int getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}

}
