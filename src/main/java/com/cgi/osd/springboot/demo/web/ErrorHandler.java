package com.cgi.osd.springboot.demo.web;

import java.util.concurrent.CompletionException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.cgi.osd.springboot.demo.service.NoCustomerException;
import com.cgi.osd.springboot.demo.service.NoFreeSeatException;

/**
 * This class is responsible for handling exceptions thrown by services. The exceptions are converted to relevant error
 * information that is sent to the REST client.
 *
 */
@ControllerAdvice
public class ErrorHandler {

	@ExceptionHandler(CompletionException.class)
	public ResponseEntity<ErrorDTO> completionExceptionHandler(CompletionException exception) {
		ResponseEntity<ErrorDTO> result;
		final Throwable cause = exception.getCause();
		if (cause instanceof NoCustomerException) {
			result = noCustomerExceptionHandler((NoCustomerException) cause);
		} else if (cause instanceof NoFreeSeatException) {
			result = noFreeSeatExceptionHandler((NoFreeSeatException) cause);
		} else {
			result = exceptionHandler((Exception) cause);
		}
		return result;
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorDTO> exceptionHandler(Exception exception) {
		final String errorMessage = "Internal server error: " + exception.getMessage();
		final ErrorDTO errorDTO = new ErrorDTO(HttpStatus.INTERNAL_SERVER_ERROR.value(), errorMessage);
		return new ResponseEntity<>(errorDTO, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(NoCustomerException.class)
	public ResponseEntity<ErrorDTO> noCustomerExceptionHandler(NoCustomerException exception) {
		final String errorMessage = "Customer number doesn't exist: " + exception.getCustomerNumber();
		final ErrorDTO errorDTO = new ErrorDTO(HttpStatus.BAD_REQUEST.value(), errorMessage);
		return new ResponseEntity<>(errorDTO, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NoFreeSeatException.class)
	public ResponseEntity<ErrorDTO> noFreeSeatExceptionHandler(NoFreeSeatException exception) {
		final String errorMessage = exception.getRequestedNumber() + " seats requested but only "
				+ exception.getActualNumber() + " free seats exists";
		final ErrorDTO errorDTO = new ErrorDTO(HttpStatus.BAD_REQUEST.value(), errorMessage);
		return new ResponseEntity<>(errorDTO, HttpStatus.BAD_REQUEST);
	}

}
