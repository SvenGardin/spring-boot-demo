package com.cgi.osd.springboot.demo.web;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cgi.osd.springboot.demo.audit.DurationLogger;
import com.cgi.osd.springboot.demo.domain.BookingDO;
import com.cgi.osd.springboot.demo.domain.CustomerDO;
import com.cgi.osd.springboot.demo.service.BookingService;

@RestController
@CrossOrigin
@RequestMapping(value = "v1.0/booking")
public class BookingController {

	@Autowired
	private BookingService bookingService;

	@PostMapping("create")
	@DurationLogger
	public void createBookings(@RequestBody BookingRequestDTO request) {
		bookingService.createBookings(request.getCustomerNumber(), request.getNumberOfSeats());
	}

	@PostMapping("/async/create")
	@DurationLogger
	@SuppressWarnings("squid:S1488")
	public Callable<Void> createBookingsAsync(@RequestBody BookingRequestDTO request) {
		final Callable<Void> dummyResult = () -> {
			bookingService.createBookings(request.getCustomerNumber(), request.getNumberOfSeats());
			return null;
		};
		return dummyResult;
	}

	@PostMapping("/async/pool/create")
	@DurationLogger
	public CompletableFuture<Void> createBookingsAsyncPool(@RequestBody BookingRequestDTO request) {
		return bookingService.createBookingsAsync(request.getCustomerNumber(), request.getNumberOfSeats());
	}

	@GetMapping("/customers")
	@DurationLogger
	public List<CustomerDO> getAllCustomers() {
		return bookingService.getAllCustomers();
	}

	@GetMapping("/bookings/{customerNumber}/{maxLimit}")
	@DurationLogger
	public Collection<BookingDO> getBookings(@PathVariable int customerNumber, @PathVariable int maxLimit) {
		return bookingService.getBookings(customerNumber, maxLimit);
	}

	@GetMapping("/async/bookings/{customerNumber}/{maxLimit}")
	@DurationLogger
	public Callable<Collection<BookingDO>> getBookingsAsync(@PathVariable int customerNumber,
			@PathVariable int maxLimit) {
		return () -> bookingService.getBookings(customerNumber, maxLimit);
	}

	@GetMapping("/bookings/paged/{customerNumber}")
	@DurationLogger
	public Page<BookingDO> getPagedBookings(@PathVariable int customerNumber, Pageable page) {
		return bookingService.getPagedBookings(customerNumber, page);
	}

}
