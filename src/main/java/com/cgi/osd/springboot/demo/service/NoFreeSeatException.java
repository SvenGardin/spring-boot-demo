package com.cgi.osd.springboot.demo.service;

public class NoFreeSeatException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final int requestedNumber;
	private final int actualNumber;

	public NoFreeSeatException(int requestedNumber, int actualNumber) {
		this.requestedNumber = requestedNumber;
		this.actualNumber = actualNumber;
	}

	public int getActualNumber() {
		return actualNumber;
	}

	public int getRequestedNumber() {
		return requestedNumber;
	}

}
