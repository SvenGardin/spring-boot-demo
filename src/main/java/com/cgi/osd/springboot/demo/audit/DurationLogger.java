package com.cgi.osd.springboot.demo.audit;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * This annotation activates execution time logging.
 *
 */
@Retention(RUNTIME)
@Target({ METHOD })
public @interface DurationLogger {

}
