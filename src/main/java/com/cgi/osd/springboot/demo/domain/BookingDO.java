package com.cgi.osd.springboot.demo.domain;

import java.time.ZonedDateTime;

public class BookingDO {

	private int customerNumber;
	private int seatNumber;
	private ZonedDateTime bookingDate;

	public ZonedDateTime getBookingDate() {
		return bookingDate;
	}

	public int getCustomerNumber() {
		return customerNumber;
	}

	public int getSeatNumber() {
		return seatNumber;
	}

	public void setBookingDate(ZonedDateTime bookingDate) {
		this.bookingDate = bookingDate;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}

	public void setSeatNumber(int seatNumber) {
		this.seatNumber = seatNumber;
	}

}
