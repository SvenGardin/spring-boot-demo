package com.cgi.osd.springboot.demo.domain;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

import com.cgi.osd.springboot.demo.repository.Customer;
import com.cgi.osd.springboot.demo.repository.Seat;

@Mapper(componentModel = "spring")
public interface RepositoryMapper {

	Customer customerDoToCustomer(CustomerDO customerDO);

	List<CustomerDO> customersToCustomerDOs(List<Customer> customer);

	CustomerDO customerToCustomerDO(Customer customer);

	Seat seatDoToSeat(SeatDO seatDO);

	List<BookingDO> seatsToBookingDOs(List<Seat> seat);

	default Page<BookingDO> seatsToPagedBookingDOs(Page<Seat> seatPage) {
		return seatPage.map(this::seatToBookingDO);
	}

	@Mapping(target = "customerNumber", source = "customer.customerNumber")
	@Mapping(target = "bookingDate", source = "updateDate")
	BookingDO seatToBookingDO(Seat seat);

	SeatDO seatToSeatDO(Seat seat);

}
