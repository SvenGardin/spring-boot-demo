import { Seat } from './seat';

export class SeatPage {

    public content: Seat[];
    public totalPages: number;
    public totalElements: number;
    public last: boolean;
    public size: number;
    public numberOfElements: number;
    public first: boolean;
}
