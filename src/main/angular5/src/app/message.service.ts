import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Customer } from './customer';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MessageService {

  private customerSubject = new Subject<Customer>();
  private bookingsChangedSubject = new Subject<boolean>();

  constructor() { }

  public sendBookingsChanged() {
    this.bookingsChangedSubject.next(true);
  }

  public getBookingsChanged(): Observable<boolean> {
    return this.bookingsChangedSubject.asObservable();
  }

  public sendCustomerMessage(message: Customer) {
    this.customerSubject.next(message);
  }

  public getCustomerMessage(): Observable<Customer> {
    return this.customerSubject.asObservable();
  }
 }
