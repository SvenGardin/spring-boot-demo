import { Component, OnInit } from '@angular/core';

import { DbService } from '../db.service';
import { Customer } from '../customer';
import { MessageService } from '../message.service';
import { ControllerBase } from '../controller-base';

@Component({
  selector: 'app-select-customer',
  templateUrl: './select-customer.component.html',
  styleUrls: ['./select-customer.component.css']
})
export class SelectCustomerComponent extends ControllerBase implements OnInit {

  public customers: Customer[];
  public selectedCustomer: Customer;

  constructor(private dbService: DbService, private messageService: MessageService) {
    super();
  }

  public onCustomerSelected(event: any) {
    this.selectedCustomer = event.value;
    this.messageService.sendCustomerMessage(this.selectedCustomer);
    console.log('Customer selected');
  }

  ngOnInit() {
    this.dbService.getAllCustomers().subscribe(
      data => this.handleData(data),
      err => this.handleError(err)
    );
  }

  private handleData(data: Customer[]) {
    this.customers = data;
  }

}
