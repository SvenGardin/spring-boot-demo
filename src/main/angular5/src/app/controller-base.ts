import { Message } from 'primeng/components/common/api';
import { BackendErrorMessage } from './backend-error-message';

export class ControllerBase {

    public msgs: Message[] = [];
    public get isMsgs(): boolean {
        return this.msgs.length !== 0;
    }

    protected handleError(err: Object) {
        let errorMessage: string;
        if (err instanceof BackendErrorMessage) {
            const backendError = err;
            errorMessage = backendError.errorMessage;
        } else {
            errorMessage = err as string;
        }
        this.msgs.push({ severity: 'error', summary: 'Backend failed', detail: errorMessage });
        console.error(errorMessage);
    }

}
