import { Component, OnInit } from '@angular/core';

import { DbService } from '../db.service';
import { MessageService } from '../message.service';
import { Customer } from '../customer';
import { Subscription } from 'rxjs/Subscription';
import { ControllerBase } from '../controller-base';

@Component({
  selector: 'app-create-bookings',
  templateUrl: './create-bookings.component.html',
  styleUrls: ['./create-bookings.component.css']
})
export class CreateBookingsComponent extends ControllerBase {

  public numberOfSeats: number;
  public customer: Customer;

  private subscription: Subscription;

  constructor(private dbService: DbService, private messageService: MessageService) {
    super();
    this.subscription = this.messageService.getCustomerMessage().subscribe(customer => this.onCustomerSelected(customer));
  }

  public createBooking() {
    this.dbService.bookSeats(this.customer.customerNumber, this.numberOfSeats).subscribe(
      data => { this.onBookingCreated(); },
      err => this.handleError(err)
    );
  }

  private onCustomerSelected(customer: Customer) {
    this.customer = customer;
  }

  private onBookingCreated() {
    this.messageService.sendBookingsChanged();
    this.msgs = [];
  }
}
