import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Seat } from './seat';
import { SeatPage } from './seat-page';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { BackendErrorMessage } from './backend-error-message';
import { Customer } from './customer';
import { isDevMode } from '@angular/core';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class DbService {

  private readonly apiRoot;

  constructor(private http: HttpClient) {
    if (isDevMode()) {
      this.apiRoot = 'http://127.0.0.1:8080';
    } else {
      this.apiRoot = '';
    }
  }

  public getAllCustomers(): Observable<Customer[]> {
    const url = `${this.apiRoot}/v1.0/booking/customers`;
    return this.http.get<Customer[]>(url).pipe(catchError(this.handleError));
  }

  public getSeats(customer: Customer, page: number, pageSize: number): Observable<SeatPage> {
    const url = `${this.apiRoot}/v1.0/booking/bookings/paged/${customer.customerNumber}/?page=${page}&size=${pageSize}`;
    return this.http.get<SeatPage>(url).pipe(catchError(this.handleError));
  }

  public bookSeats(customerNumber: number, numberOfSeats: number): Observable<Object> {
    const url = `${this.apiRoot}/v1.0/booking/create`;
    return this.http.post<Object>(url, {
      customerNumber: customerNumber,
      numberOfSeats: numberOfSeats
    }).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      return new ErrorObservable(`An error occurred: ${error.error.message}`);
    }

    if (error.error instanceof ProgressEvent) {
      return new ErrorObservable(`A connection error occurred: ${error.message}`);
    }

    const errorMessage = new BackendErrorMessage();
    if (error.error != null) {
      errorMessage.errorCode = error.error.errorCode;
      errorMessage.errorMessage = error.error.errorMessage;
    } else {
      errorMessage.errorCode = error.status;
      errorMessage.errorMessage = error.statusText;
    }
     return new ErrorObservable(errorMessage);
  }

}
