export class BackendErrorMessage {

    public errorCode: number;
    public errorMessage: string;
}
