import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { ButtonModule } from 'primeng/button';
import { KeyFilterModule } from 'primeng/keyfilter';
import { PanelModule } from 'primeng/panel';
import { DropdownModule } from 'primeng/dropdown';
import { MessageModule } from 'primeng/message';
import {MessagesModule} from 'primeng/messages';

import { AppComponent } from './app.component';
import { BookedSeatsComponent } from './booked-seats/booked-seats.component';
import { DbService } from './db.service';
import { CreateBookingsComponent } from './create-bookings/create-bookings.component';
import { SelectCustomerComponent } from './select-customer/select-customer.component';
import { MessageService } from './message.service';

@NgModule({
  declarations: [
    AppComponent,
    BookedSeatsComponent,
    CreateBookingsComponent,
    SelectCustomerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TableModule,
    PaginatorModule,
    ButtonModule,
    KeyFilterModule,
    PanelModule,
    DropdownModule,
    MessageModule,
    MessagesModule
   ],
  providers: [
    DbService, MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
