import { Component, OnInit } from '@angular/core';

import { DbService } from '../db.service';
import { Seat } from '../seat';
import { SeatPage } from '../seat-page';
import { Customer } from '../customer';
import { MessageService } from '../message.service';
import { Subscription } from 'rxjs/Subscription';
import { ControllerBase } from '../controller-base';

@Component({
  selector: 'app-booked-seats',
  templateUrl: './booked-seats.component.html',
  styleUrls: ['./booked-seats.component.css']
})
export class BookedSeatsComponent extends ControllerBase {

  public bookedSeats: Seat[];
  public totalElements = 0;
  public pageSize = 20;

  private customer: Customer;
  private customerSubscription: Subscription;
  private bookingsChangedSubscription: Subscription;

  constructor(private dbService: DbService, private messageService: MessageService) {
    super();
    this.customerSubscription = this.messageService.getCustomerMessage().subscribe(customer => this.onCustomerSelected(customer));
    this.bookingsChangedSubscription = this.messageService.getBookingsChanged().subscribe(changed => this.getBookedSeats(0));
  }

  getBookedSeats(pageNumber: number) {
    this.dbService.getSeats(this.customer, pageNumber, this.pageSize).subscribe(
      data => this.assignSeats(data),
      err => this.handleError(err)
    );
  }

  public paginate(event) {
     this.getBookedSeats(event.page);
  }

  public dummySort(event) {
    // Work-around for prime-ng bug
  }

  private assignSeats(page: SeatPage) {
    this.totalElements = page.totalElements;
    this.bookedSeats = page.content;
   }

  private onCustomerSelected(customer: Customer) {
    this.customer = customer;
    this.getBookedSeats(0);
  }

}
