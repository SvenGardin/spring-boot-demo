package com.cgi.osd.springboot.demo.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cgi.osd.springboot.demo.TestUtils;
import com.cgi.osd.springboot.demo.domain.BookingDO;
import com.cgi.osd.springboot.demo.domain.CustomerDO;
import com.cgi.osd.springboot.demo.service.BookingService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-jpa.properties")
@AutoConfigureTestEntityManager
@AutoConfigureTestDatabase
public class BookingControllerTests {

	@Autowired
	private TestEntityManager testEntityManager;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private BookingService bookingService;

	@Autowired
	private TestUtils testUtils;

	@Test
	public void createBookingsAsyncPoolTest() {
		performCreateBookingsTest("/v1.0/booking/async/pool/create", 5, 10);
	}

	@Test
	public void createBookingsAsyncTest() {
		performCreateBookingsTest("/v1.0/booking/async/create", 5, 10);
	}

	@Test
	public void createBookingsTest() {
		performCreateBookingsTest("/v1.0/booking/create", 5, 10);
	}

	@Test
	public void getAllCustomersTest() {
		final ParameterizedTypeReference<List<CustomerDO>> parameterizedTypeReference = new ParameterizedTypeReference<List<CustomerDO>>() {
		};
		final ResponseEntity<List<CustomerDO>> response = restTemplate.exchange("/v1.0/booking/customers",
				HttpMethod.GET, null, parameterizedTypeReference);
		final List<CustomerDO> customers = response.getBody();

		assertEquals(10, customers.size());
	}

	@Test
	public void getBookingsAsyncTest() {
		performGetBookingsTest("/v1.0/booking/async/bookings/");
	}

	@Test
	public void getBookingsTest() {
		performGetBookingsTest("/v1.0/booking/bookings/");
	}

	@Before
	public void initTables() {
		testUtils.initTablesInNewTransaction(testEntityManager);
	}

	@Test
	public void noCustomerTest() {
		performNoCustomerTest("/v1.0/booking/create");
	}

	@Test
	public void noCustomerTestAsync() {
		performNoCustomerTest("/v1.0/booking/async/create");
	}

	@Test
	public void noCustomerTestAsyncPool() {
		performNoCustomerTest("/v1.0/booking/async/pool/create");
	}

	@Test
	public void noFreeSeatTest() {
		performNoFreeSeatTest("/v1.0/booking/create");
	}

	@Test
	public void noFreeSeatTestAsync() {
		performNoFreeSeatTest("/v1.0/booking/async/create");
	}

	@Test
	public void noFreeSeatTestAsyncPool() {
		performNoFreeSeatTest("/v1.0/booking/async/pool/create");
	}

	private ResponseEntity<ErrorDTO> performCreateBookingsTest(String path, int customerNumber, int numberOfBookings) {
		final BookingRequestDTO request = new BookingRequestDTO(customerNumber, numberOfBookings);
		final HttpEntity<BookingRequestDTO> requestEntity = new HttpEntity<>(request);

		final ResponseEntity<ErrorDTO> response = restTemplate.postForEntity(path, requestEntity, ErrorDTO.class);
		return response;
	}

	private void performGetBookingsTest(String path) {
		final int customerNumber = 1;
		final int numberOfBookings = 5;
		bookingService.createBookings(customerNumber, numberOfBookings);
		final ParameterizedTypeReference<Collection<BookingDO>> parameterizedTypeReference = new ParameterizedTypeReference<Collection<BookingDO>>() {
		};

		final String completePath = path + customerNumber + "/100";
		final ResponseEntity<Collection<BookingDO>> response = restTemplate.exchange(completePath, HttpMethod.GET, null,
				parameterizedTypeReference);
		final Collection<BookingDO> bookings = response.getBody();

		Assert.assertEquals(numberOfBookings, bookings.size());
	}

	private void performNoCustomerTest(String path) {
		final int customerNumber = 11;
		final ResponseEntity<ErrorDTO> response = performCreateBookingsTest("/v1.0/booking/create", customerNumber, 10);
		final ErrorDTO expectedErrorDTO = new ErrorDTO(HttpStatus.BAD_REQUEST.value(),
				"Customer number doesn't exist: " + customerNumber);
		final ErrorDTO actualErrorDTO = response.getBody();

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertTrue(expectedErrorDTO.equals(actualErrorDTO));
	}

	private void performNoFreeSeatTest(String path) {
		final int requestedSeats = 101;
		final ResponseEntity<ErrorDTO> response = performCreateBookingsTest(path, 5, requestedSeats);
		final ErrorDTO expectedErrorDTO = new ErrorDTO(HttpStatus.BAD_REQUEST.value(),
				requestedSeats + " seats requested but only 100 free seats exists");
		final ErrorDTO actualErrorDTO = response.getBody();

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertTrue(expectedErrorDTO.equals(actualErrorDTO));
	}

};