package com.cgi.osd.springboot.demo.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cgi.osd.springboot.demo.TestUtils;

@RunWith(SpringRunner.class)
@DataJpaTest(showSql = false)
@TestPropertySource(locations = "classpath:application-test-jpa.properties")
@ContextConfiguration(locations = "classpath:unit-test-context.xml")
public class RepositoryTests {

	@Autowired
	private TestEntityManager testEntityManager;

	@Autowired
	private TestUtils testUtils;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private SeatRepository seatRepository;

	@Test
	public void checkIfSeatFreeTest() {
		final int bookedSeatNumber = 50;
		testUtils.createBooking(10, bookedSeatNumber);

		Assert.assertTrue(seatRepository.isSeatFree(bookedSeatNumber));
		Assert.assertFalse(seatRepository.isSeatFree(49));
	}

	@Test
	public void findAllCustomersTest() {
		final List<Customer> customers = customerRepository.findAll();

		assertEquals(10, customers.size());
	}

	@Test
	public void findCustomerBookingsTest() {
		final int customerNumber = 5;
		final int[] seatNumbers = { 50, 51, 52 };

		for (final int seatNumber : seatNumbers) {
			testUtils.createBooking(customerNumber, seatNumber);
		}

		final List<Seat> seats = seatRepository.findByCustomer(customerNumber, PageRequest.of(0, 100)).getContent();
		for (int i = 0; i < seatNumbers.length; i++) {
			assertEquals(seatNumbers[i], seats.get(seatNumbers.length - i - 1).getSeatNumber());
		}
	}

	@Test
	public void findCustomerTest() {
		final Customer customer = customerRepository.findByCustomerNumber(10).get();

		Assert.assertEquals(10, customer.getCustomerNumber());
	}

	@Test
	public void findFreeSeatTest() {
		testUtils.createBooking(10, 100);

		final List<Seat> freeSeats = seatRepository.findFreeSeats(PageRequest.of(0, 100)).getContent();

		Assert.assertEquals(99, freeSeats.size());
	}

	@Test
	public void findNoCustomerTest() {
		final Optional<Customer> customer = customerRepository.findByCustomerNumber(11);
		Assert.assertTrue(!customer.isPresent());
	}

	@Test
	public void findNoFreeSeatTest() {
		for (int i = 1; i < 101; i++) {
			testUtils.createBooking(10, i);
		}

		final List<Seat> freeSeats = seatRepository.findFreeSeats(PageRequest.of(0, 100)).getContent();

		Assert.assertTrue(freeSeats.isEmpty());
	}

	@Test
	public void findNumberOfFreeSeatsTest() {
		final long numberOfFreeSeats = seatRepository.countByCustomerIsNull();
		assertEquals(100, numberOfFreeSeats);
	}

	@Test
	public void findSeatTest() {
		final Optional<Seat> optionalSeat = seatRepository.findBySeatNumber(100);

		Assert.assertTrue(optionalSeat.isPresent());
	}

	@Before
	public void initTables() {
		testUtils.initTablesInNewTransaction(testEntityManager);
	}

	@Test
	public void limitBookedSeatsTest() {
		final int customerNumber = 10;
		for (int i = 1; i < 101; i++) {
			testUtils.createBooking(customerNumber, i);
		}

		final int numberOfSeats = 50;
		final List<Seat> seats = seatRepository.findByCustomer(customerNumber, PageRequest.of(0, numberOfSeats))
				.getContent();

		assertEquals(numberOfSeats, seats.size());
	}

	@Test
	public void persistBookingTest() {
		final int customerNumber = 10;
		final int seatNumber = 100;
		testUtils.createBooking(customerNumber, seatNumber);

		assertTrue(seatRepository.findBySeatNumber(seatNumber).get().isBooked());
	}

	@Test
	public void persistCustomerTest() {
		final Customer customer = new Customer();
		customer.setCustomerNumber(11);
		customerRepository.save(customer);

		final Optional<Customer> optionalCustomer = customerRepository.findByCustomerNumber(11);
		Assert.assertTrue(optionalCustomer.isPresent());
		Assert.assertEquals(11, optionalCustomer.get().getCustomerNumber());
		testUtils.validateUpdateDate(optionalCustomer.get());
	}

}
