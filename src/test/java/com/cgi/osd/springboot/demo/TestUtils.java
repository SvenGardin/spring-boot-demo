package com.cgi.osd.springboot.demo;

import java.time.Instant;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cgi.osd.springboot.demo.repository.Customer;
import com.cgi.osd.springboot.demo.repository.CustomerRepository;
import com.cgi.osd.springboot.demo.repository.EntityBase;
import com.cgi.osd.springboot.demo.repository.Seat;
import com.cgi.osd.springboot.demo.repository.SeatRepository;

@Component
public class TestUtils {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private SeatRepository seatRepository;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void initTablesInNewTransaction(TestEntityManager testEntityManager) {
		initTables(testEntityManager);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createBooking(int customerNumber, int seatNumber) {
		final Seat seat = seatRepository.findBySeatNumber(seatNumber).get();
		final Customer customer = customerRepository.findByCustomerNumber(customerNumber).get();
		seat.setCustomer(customer);
	}

	public void validateUpdateDate(EntityBase entity) {
		final Instant now = Instant.now();
		final Instant updateTime = entity.getUpdateDate().toInstant();
		final Instant updateDiff = now.minusMillis(updateTime.toEpochMilli());
		Assert.assertTrue(1 > updateDiff.getEpochSecond());
	}

	public void validateCreateDate(EntityBase entity) {
		final Instant now = Instant.now();
		final Instant updateTime = entity.getCreateDate().toInstant();
		final Instant updateDiff = now.minusMillis(updateTime.toEpochMilli());
		Assert.assertTrue(1 > updateDiff.getEpochSecond());
	}

	private void initTables(TestEntityManager testEntityManager) {
		final EntityManager em = testEntityManager.getEntityManager();

		em.createNativeQuery("DELETE FROM seat").executeUpdate();
		em.createNativeQuery("DELETE FROM customer").executeUpdate();

		final Query seatQuery = em.createNativeQuery(
				"INSERT INTO `seat` (create_date, update_date, version, seat_number) VALUES(NOW(), NOW(), 0, :seatNumber)");
		for (int i = 0; i < 100; i++) {
			seatQuery.setParameter("seatNumber", i + 1);
			seatQuery.executeUpdate();
		}

		final Query customerQuery = em.createNativeQuery(
				"INSERT INTO `customer` (create_date, update_date, version, customer_number) VALUES(NOW(), NOW(), 0, :customerNumber)");
		for (int i = 0; i < 10; i++) {
			customerQuery.setParameter("customerNumber", i + 1);
			customerQuery.executeUpdate();
		}
	}

}
