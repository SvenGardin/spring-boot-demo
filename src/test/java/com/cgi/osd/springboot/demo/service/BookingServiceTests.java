package com.cgi.osd.springboot.demo.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.cgi.osd.springboot.demo.TestUtils;
import com.cgi.osd.springboot.demo.domain.BookingDO;
import com.cgi.osd.springboot.demo.domain.CustomerDO;

@RunWith(SpringRunner.class)
@DataJpaTest(showSql = false)
@TestPropertySource(locations = "classpath:application-test-jpa.properties")
@ContextConfiguration(locations = "classpath:unit-test-context.xml")
public class BookingServiceTests {

	@Autowired
	private BookingService bookingService;

	@Autowired
	private TestEntityManager testEntityManager;

	@Autowired
	private TestUtils testUtils;

	@Test
	public void createBookingsTest() {
		final int customerNumber = 1;
		final int numberOfBookings = 3;
		bookingService.createBookings(1, numberOfBookings);
		final Collection<BookingDO> bookings = bookingService.getBookings(customerNumber, 100);
		assertEquals(numberOfBookings, bookings.size());
	}

	@Test
	public void getAllCustomersTest() {
		final List<CustomerDO> customers = bookingService.getAllCustomers();
		assertEquals(10, customers.size());
	}

	@Test
	public void getBookingsNoCustomerTest() {
		final int customerNumber = 11;
		try {
			bookingService.getBookings(customerNumber, 100);
		} catch (final NoCustomerException ex) {
			assertEquals(customerNumber, ex.getCustomerNumber());
			return;
		}
		fail("No NoCustomerException thrown");
	}

	@Test
	public void getBookingsPageNoCustomerTest() {
		final int customerNumber = 11;
		try {
			bookingService.getPagedBookings(customerNumber, PageRequest.of(0, 10));
		} catch (final NoCustomerException ex) {
			assertEquals(customerNumber, ex.getCustomerNumber());
			return;
		}
		fail("No NoCustomerException thrown");
	}

	@Test
	public void getBookingsPageTest() {
		final int customerNumber = 1;
		final int numberOfBookings = 50;
		final int pageSize = 5;
		bookingService.createBookings(customerNumber, numberOfBookings);
		final Page<BookingDO> bookingsPage = bookingService.getPagedBookings(customerNumber,
				PageRequest.of(0, pageSize));

		assertEquals(numberOfBookings, bookingsPage.getTotalElements());
		assertEquals(pageSize, bookingsPage.getContent().size());
	}

	@Test
	public void getBookingsTest() {
		final int customerNumber = 1;
		final int numberOfBookings = 5;
		bookingService.createBookings(customerNumber, numberOfBookings);
		final Collection<BookingDO> bookings = bookingService.getBookings(customerNumber, 100);
		assertEquals(numberOfBookings, bookings.size());
	}

	@Before
	public void initTables() {
		testUtils.initTablesInNewTransaction(testEntityManager);
	}

	@Test
	public void limitBookingsTest() {
		final int customerNumber = 1;
		final int numberOfBookings = 100;
		final int maxLimit = 50;
		bookingService.createBookings(customerNumber, numberOfBookings);
		final Collection<BookingDO> bookings = bookingService.getBookings(customerNumber, maxLimit);
		assertEquals(maxLimit, bookings.size());
	}

	@Test
	public void noCustomerTest() {
		final int customerNumber = 11;
		try {
			bookingService.createBookings(customerNumber, 1);
		} catch (final NoCustomerException ex) {
			assertEquals(customerNumber, ex.getCustomerNumber());
			return;
		}
		fail("No NoCustomerException thrown");
	}

	@Test
	public void noFreeSeatTest() {
		try {
			bookingService.createBookings(5, 101);
		} catch (final NoFreeSeatException ex) {
			assertEquals(101, ex.getRequestedNumber());
			assertEquals(100, ex.getActualNumber());
			return;
		}
		fail("No NoFreeSeatException thrown");

	}

}
